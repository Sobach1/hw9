FROM python:3.9

RUN pip install \
    flask \
    numpy \
    pandas \
    sklearn \
    boto3 \
    mlflow==1.14.1 \
    psycopg2-binary 

COPY app.py /
COPY thresholds.json /

EXPOSE 8080/tcp

CMD ["python3", "app.py"]
