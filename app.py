import os
import mlflow
import json
import numpy as np
import pandas as pd
import sklearn
from flask import Flask, jsonify, request, abort

app = Flask(__name__)

@app.route("/get_source_iris_pred")
def simple_prediction():
    sepal_length = request.args.get("sepal_length")
    sepal_width = request.args.get("sepal_width")
    float_prob = model.predict(np.array([[sepal_length, sepal_width]]))[0]
    return jsonify(prediction=float_prob)

@app.route("/get_string_iris_pred")
def complicated_prediction():
    if os.getenv("IS_INITIAL_RUN"):
        abort(501)
    sepal_length = request.args.get("sepal_length")
    sepal_width = request.args.get("sepal_width")
    float_prob = model.predict(np.array([[sepal_length, sepal_width]]))[0]
    if float_prob < thresholds["threshold_0_1"]:
        pred_class = "setosa"
    elif float_prob < thresholds["threshold_1_2"]:
        pred_class = "versicolor"
    else:
        pred_class = "virginica"
    return jsonify({
        "class":float_prob,
        "class_str":pred_class,
        "threshold_0_1":thresholds["threshold_0_1"],
        "threshold_1_2":thresholds["threshold_1_2"],
    })

if __name__ == "__main__":
    os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://95.217.1.97:19001"
    os.environ["MLFLOW_TRACKING_URI"] = "http://95.217.1.97:5002"
    os.environ["AWS_ACCESS_KEY_ID"] = "IAM_ACCESS_KEY"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "IAM_SECRET_KEY"

    mlflow.set_tracking_uri("http://95.217.1.97:5002")
    client = mlflow.tracking.MlflowClient()
    model = mlflow.sklearn.load_model("models:/hw9/production")

    try:
        with open("thresholds.json", "r") as f:
            thresholds = json.load(f)
    except:
        thresholds = {"threshold_0_1": 0.5, "threshold_1_2": 1.5}

    app.run(host="0.0.0.0", port=8080)
